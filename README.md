# visages de traître

## regarder et écouter

There is a new arms race within the USA.  
Rather than gunpowder, brass, and lead,  
the bullets are composed of coarsely duplicated photons,  
and the receiver is made of algorithims instead of steel.

It's just another tool  
whose purpose is justified  
by its handler.

Today, I rejoice.  
The insurrectionists will be remembered  
appropriately.

In the future, I worry:  
How will they use what we've built  
upon their next victory?

https://www.vice.com/en/article/xgz7g7/facial-recognition-parler-videos  
https://www.nytimes.com/2021/01/09/technology/facial-recognition-clearview-capitol.html

Pull requests welcome!
