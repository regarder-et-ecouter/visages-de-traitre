#!/usr/bin/python3

import os

search_faces = os.listdir(os.path.join(os.path.abspath('.'), 'needles'))
known_faces = os.listdir(os.path.join(os.path.abspath('.'), 'haystack'))

if __name__ == '__main__':
    print('"The best is yet to come," she said...')
